% Pour compiler ce fichier, il est nécessaire de recourir à l'un des
% compilateurs `xelatex' ou `lualatex'.

\RequirePackage[2020-02-02]{latexrelease}
\documentclass[french]{article}
\usepackage{fontspec}
\usepackage{libertine}
\setmonofont{Latin Modern Mono}
%
\newcommand{\keystroke}[1]{\LKey{#1}}
\newcommand{\Ctrl}{\LKeyCtrl}
\newcommand{\Shift}{\LKeyShift}
\newcommand{\Alt}{\LKeyAlt}
\newcommand{\Back}{\LKeyBack}
\newcommand{\LArrow}{\LKeyLeft}
\newcommand{\RArrow}{\LKeyRight}
\newcommand{\DArrow}{\LKeyDown}
\newcommand{\UArrow}{\LKeyUp}
\newcommand{\Home}{\LKeyHome}
\newcommand{\End}{\LKeyEnd}
\newcommand{\PgDown}{\LKeyScreenDown}
\newcommand{\PgUp}{\LKeyScreenUp}
\newcommand{\Tab}{\LKeyTab}
\newcommand{\Esc}{\LKeyEsc}
%
\usepackage{booktabs}
\usepackage[a4paper]{geometry}
\usepackage{longtable}
\usepackage{babel}
\usepackage{hyperref}
\usepackage{menukeys}
%
\newcommand{\software}[1]{\textsf{#1}}
\newcommand{\linux}{\software{Linux}}
\newcommand{\win}{\software{Windows}}
\newcommand{\txs}{\software{TeXstudio}}
\newcommand{\emptycell}{\multicolumn{1}{r@{}}{}}
%
\newcommand{\fkey}[1]{%
  \ifPDFTeX%
  \keystroke{F#1}%
  \else%
  \LKeyF{#1}%
  \fi%
}
\newcommand{\menukey}{%
  \ifPDFTeX%
  \keystroke{touche menu}%
  \else%
  \LKeyMenu%
  \fi%
}
\newcommand{\winkey}{%
  \ifPDFTeX%
  \keystroke{touche \win{}}%
  \else%
  \LKeyWin%
  \fi%
}
\newcommand{\enterkey}{%
  \ifPDFTeX%
  \Enter%
  \else%
  \LKeyEnter%
  \fi%
}
\newcommand{\meta}[1]{%
  $\langle$\texttt{#1}$\rangle$%
}
%
\begin{document}
\title{Raccourcis claviers communs à de nombreux outils et sur plusieurs
  systèmes d'exploitation}
\author{Denis \textsc{Bitouzé} \& Jean-Bernard \textsc{Choquel}}
\maketitle
\begin{abstract}
  Nombreux sont ceux qui, sur leur ordinateur, effectuent l'essentiel de leurs
  tâches à l'aide de la souris et ne recourrent que rarement, voire jamais, aux
  raccourcis claviers. Sans qu'ils en aient forcément conscience, ceci finit par
  leur faire perdre un temps non négligeable et est susceptible d'augmenter
  significativement leur stress.

  Ce court document liste quelques raccourcis claviers connus et moins connus
  permettant au lecteur de minimiser le recours à la souris et de, ainsi, gagner
  en efficacité.
\end{abstract}
\tableofcontents

\section{Introduction}
\label{sec:introduction}

Ce document\footnote{Dont le source est disponible à l'adresse
  \url{https://gogs.univ-littoral.fr/bitouze/shortcuts}.} liste un certain
nombre de raccourcis claviers connus et moins connus. Son but est de permettre
au lecteur de gagner en efficacité en :
\begin{itemize}
\item maximisant le nombre de tâches effectuées au clavier (précis) ;
\item minimisant le nombre de tâches effectuées la souris (peu précise), ce
  qui fait perdre du temps à :
  \begin{enumerate}
  \item lâcher le clavier pour atteindre la souris ;
  \item faire bouger le pointeur pour repérer sa position sur l'écran ;
  \item amener ce pointeur à l'endroit ou aux endroits successifs\footnote{Par
      exemple en cas de navigation dans des menus puis sous-menus, etc.} voulus de
    l'écran ;
  \item cliquer sur un bouton ;
  \item lâcher la souris pour revenir le clavier.
  \end{enumerate}
\end{itemize}

Les raccourcis indiqués ici sont valables pour des claviers pour \textsc{pc}
standards distribués en France et pour plusieurs systèmes d'exploitation,
notamment \linux{} et \win{} ; pour les appareils de type Macintosh, plusieurs
raccourcis devraient rester valables moyennant adaptation\footnote{Cf. par
  exemple le document à l'adresse
  \url{https://support.apple.com/fr-fr/HT201236}.}.

\section{Raccourcis généraux}
\label{sec:raccourcis-generaux}

Le tableau suivant répertorie des raccourcis communs à de nombreux outils
(éditeurs de texte, traitements de texte, applications diverses).

\begin{longtable}{l@{ }p{6cm}r@{ + }l}
    \multicolumn{2}{c}{\textbf{Action}}                                                                                                                                                                                 & \multicolumn{1}{r}{(\textbf{Modificateur})} & \multicolumn{1}{@{}l}{\textbf{Touche}}                \\\toprule
\endhead
  Déplacer le curseur                                                                                                                                                                                                   & de caractère en caractère                   & \emptycell & \multicolumn{1}{@{}l}{\RArrow{} (\LArrow)} \\\cmidrule{2-4}
                                                                                                                                                                                                                        & de mot en mot                               & \Ctrl      & \RArrow{} (\LArrow)                        \\\cmidrule{2-4}
                                                                                                                                                                                                                        & en fin (début) de ligne                     & \emptycell & \multicolumn{1}{@{}l}{\End{} (\Home)}      \\\cmidrule{2-4}
                                                                                                                                                                                                                        & en fin (début) de fichier                   & \Ctrl      & \End{} (\Home)                             \\\midrule
  Sélectionner                                                                                                                                                                                                          & caractère par caractère                     & \Shift     & \RArrow{} (\LArrow)                        \\\cmidrule{2-4}
                                                                                                                                                                                                                        &
                                                                                                  mot par mot                                                                                                           & \Shift + \Ctrl                              & \RArrow{} (\LArrow)                                     \\\cmidrule{2-4}
                                                                                                                                                                                                                        & tout le document                            & \Ctrl      & \keystroke{A}                            \\\cmidrule{2-4}
                                                                                                                                                                                                                        & jusqu'en fin (début) de ligne               & \Shift     & \End{} (\Home)                            \\\cmidrule{2-4}
                                                                                                                                                                                                                        &
                                                                                                  jusqu'en fin (début) ligne, y compris le retour chariot, jusqu'à la même position de la ligne inférieure (supérieure) & \Shift                                      & \DArrow{} (\UArrow) \\\midrule
  \multicolumn{2}{l}{Couper}                                                                                                                                                                                            & \Ctrl                                       & \keystroke{X}     \\\midrule
  \multicolumn{2}{l}{Copier}                                                                                                                                                                                            & \Ctrl                                       & \keystroke{C}     \\\midrule
  \multicolumn{2}{l}{Coller}                                                                                                                                                                                            & \Ctrl                                       & \keystroke{V}     \\\midrule
  \multicolumn{2}{l}{Supprimer le mot suivant (précédent)}                                                                                                                                                                        & \Ctrl                                       & \LKeyDel{} (\Back)     \\\midrule
  \multicolumn{2}{l}{Défaire}                                                                                                                                                                                           & \Ctrl                                       & \keystroke{Z}     \\\midrule
  \multicolumn{2}{l}{Refaire}                                                                                                                                                                                           & \Ctrl                                       & \keystroke{Y}     \\\midrule
  \multicolumn{2}{l}{Rechercher}                                                                                                                                                                                        & \Ctrl                                       & \keystroke{F}     \\\midrule
  \multicolumn{2}{l}{Rechercher (poursuivre)}                                                                                                                                                                           & \emptycell                                  & \fkey{3}          \\\midrule
  \multicolumn{2}{l}{Remplacer}                                                                                                                                                                                         & \Ctrl                                       & \keystroke{R}     \\\midrule
  \multicolumn{2}{l}{Accéder aux menus (soulignement des caractères accélérateurs)}
                                                                                                                                                                                                                        &
                                                                                                  \Alt\footnote{Par
                                                                                                  exemple,
                                                                                                  dans
                                                                                                  \txs{} :
                                                                                                  \Alt+%
                                                                                                  \keystroke{L}
                                                                                                  \keystroke{L}
                                                                                                  \keystroke{N}
                                                                                                  permet
                                                                                                  d'atteindre
                                                                                                  l'entrée de
                                                                                                  menu \menu{\underline{L}aTeX > \underline{L}istes > Ordonnée (\underline{n}umérotée)}.}
                                                                                                                                                                                                   &
  \meta{lettre(s)}\\\midrule
  \multicolumn{2}{l}{Circuler entre les fenêtres ouvertes} & \Alt{} (+ \Shift) & \Tab                                                                                                                                                                                 \\\midrule
  \multicolumn{2}{l}{Ouvrir le menu contextuel} & \emptycell & \menukey                                                                                                                                                                                 \\\bottomrule
  \end{longtable}

\section{Raccourcis propres à \txs}
\label{sec:racc-propr-txs}

Le tableau suivant répertorie quelques raccourcis propres à l'éditeur de texte
orienté \LaTeX{} \txs.

\begin{longtable}{l@{ }p{5cm}r@{ + }l}
  \multicolumn{2}{c}{\textbf{Action}}                                              &
                                       \multicolumn{1}{r}{(\textbf{Modificateur})} & \multicolumn{1}{@{}l}{\textbf{Touche}} \\\toprule
  \endhead
  Compiler et visualiser                                                           &  & \emptycell &
                                                   \multicolumn{1}{@{}l}{\fkey{5}\footnote{Jusqu'à
                                                   la version 2.9.4 de
                                                   \txs{}, le raccourci pour
                                                                                                        cette action
                                                   était
                                                   \fkey{1}.}}                                                                                                    \\\midrule{}%
  (Dé)Commenter                                                                                   & la ligne en cours\footnote{Où que soit le curseur.} ou les lignes
                  sélectionnées                                                                   & \Ctrl                      & (\keystroke{U})
                                          \keystroke{T}                                                                                                           \\\midrule
  Insérer                                                                                         & un item de liste           & \Ctrl+\Shift & \keystroke{I}     \\\cmidrule{2-4}
                                                                                                  & un environnement générique & \Ctrl        & \keystroke{E}     \\\midrule
  Appliquer un style de caractères                                                                & emphase                    & \Ctrl+\Shift & \keystroke{E}     \\\cmidrule{2-4}
                                                                                                  & gras                       & \Ctrl        & \keystroke{B}     \\\cmidrule{2-4}
                                                                                                  & italique                   & \Ctrl        & \keystroke{I}     \\\cmidrule{2-4}
                                                                                                  & petites capitales
                                                                                                  &
                                                                                     \Ctrl+\Shift & \keystroke{C}                                                 \\\midrule
  Fermer quelque chose                                                                            &                            & \emptycell   & \Esc              \\\midrule
  Atteindre l'onglet suivant (précédent)
                                                                                                  &
                                                                                                  &
                                                                                     \Ctrl        & \PgDown{} (\PgUp)                                               \\\midrule
  Donner le focus à l'afficheur (l'éditeur)                                                       &                            & \Ctrl+\Alt   & \RArrow{} (\LArrow) \\\midrule
\end{longtable}

\section{Raccourcis propres à \win}
\label{sec:racc-propr-win}

Le tableau suivant répertorie quelques raccourcis fournis par le système
d'exploitation \win{}.

\begin{longtable}{l@{ }p{5cm}r@{ + }l}
  \multicolumn{2}{c}{\textbf{Action}}                                              &
                                       \multicolumn{1}{r}{(\textbf{Modificateur})} & \multicolumn{1}{@{}l}{\textbf{Touche}}  \\\toprule
  \endhead
  Ouvrir le poste de travail                                                       &  & \winkey &
                                                                                                               \keystroke{E} \\\midrule{}%
  Fermer d'une fenêtre                                                             &  & \Alt    &
                                                                                                               \fkey{4}      \\\midrule{}%
  Exécuter une commande                                                            &  & \winkey &
                                                                                                          \keystroke{R}\footnote{Par
                                                                                                  exemple,
                                                                                                  pour
                                                                                                  afficher
                                                                                                  l'invite
                                                                                                  de
                                                                                                  commande \software{MSDOS} :
                                                                                                          \winkey+\keystroke{R}+\texttt{cmd}+\enterkey.} \\\midrule{}%
\end{longtable}
% Touche Windows & \keystroke{E} &
%                                    \multicolumn{2}{l}{Lancement de l'explorateur windows}                                                                                                                                                                              \\\midrule

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: luatex
%%% TeX-master: t
%%% End:
