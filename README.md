Ce court document liste un certain nombre de raccourcis claviers connus et moins
connus qui permettent d'être plus efficace en maximisant le nombre de tâches
effectuées au clavier (précis), sans perdre du temps à le lâcher pour atteindre
la souris (peu précise).

Pour compiler le fichier `.tex`, il est nécessaire de recourir à l'un des
compilateurs `xelatex` ou `lualatex`.
